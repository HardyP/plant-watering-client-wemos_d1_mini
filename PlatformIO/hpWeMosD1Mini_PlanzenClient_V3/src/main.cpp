#include <Arduino.h>
#include <OneWire.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <EEPROM.h>
#include <setup_daten.h> // Werte ggf anpassen
#include <../../ssid_password.h> // Pfad, ssid und password anpassen
 

extern "C" 
{
#include <user_interface.h> // https://github.com/esp8266/Arduino actually tools/sdk/include
}

//#define DEBUG //"Schalter" zum aktivieren

#ifdef DEBUG
#define DEBUG_PRINT(x) Serial.print(x)
#define DEBUG_PRINTLN(x) Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTLN(x) 
#endif

const int WiFiRetryMax=500; // max 500*100ms=50s Wartezeit fuer WiFi
const int ServerEchoRetryMax=200; // max 200*100ms=20s Wartezeit auf UDP-Echo
int packetSize=0;
int ServerEchoRetry=0;
int PumpeWarteBisEin=1; // in Sekunden
const int PacketSizeMax=200;
char incomingPacket[PacketSizeMax]; // Puffer für eingehende Pakete
const int hTrockenMax=901;
const int hTrockenSoll=100;
const int hAliasMaxLength=31;

#define isBitTest false // Bei isBitTest wird bei jedem gelesenem Bit ein positiver Puls auf D3 zur Kontrolle erzeugt 
WiFiUDP Udp;
OneWire TemperaturSensor(D1); 

int LED=D4;  // LED_BUILTIN
int ChirpClock=D5;  // SCK
int ChirpDaten=D6;  // MISO
int ChirpSelect=D7;  // MOSI
//int ChirpReset=D3; // ggf Chirp-Reset
int BitTest=D3;
int isPumpe=D2; // Wenn Jumper an D2 gesteckt, keine Pumpe angeschlossen
int Pumpe=D8; // Wasserpumpe
int BitLaenge=120; // Dauer eines Bits in µs
uint32_t ChirpSelectMax=400000; 
int ChirpBitCountMax=16; // Anzahl der Bits
int ErrorCount=0;
int ErrorCountMax=8;
uint16_t hFeuchte;
bool hLeseFehler=false;
int hTemperatur;
uint16_t Gelesen;
int eeAddress=0;
bool eeGeaendert=false;
unsigned int intVCC=0;
float VCC=0.0;
int hSpannung=0;
int hWasser=0;
unsigned long WachMillis=0;
uint16_t uTrocken;
uint16_t uSchlafZeit=3*60;
uint16_t uExtraSchlafZeit=0;
int uWartenBisWasserSoll;
int uPumpZeitSoll;
bool SerialDEBUG=false;
int DeepSleepWakeUp=99; // 0=WakeUp from DeepSleep; 1=Reset oder PowerDown; -1=Reset from Error
String SensorKennung;
String LineString;
String tmp;
char uAlias[hAliasMaxLength];

struct eeObject
{
  bool SerialDEBUG;  
  uint16_t hTrocken;
  int hWartenBisWasser;
  uint16_t hSchlafZeit;
  int hPumpZeitSoll;
  int hWartenBisWasserSoll;
  char hAlias[hAliasMaxLength];
};
eeObject eeVar; 

void ggfDEBUG()
{
  int ggfDEBUG_Retry=0;
  while (ggfDEBUG_Retry<2000) 
  {
    if (Serial.available()>0)
    {
      SerialDEBUG=true;
      break;
    }
    delay(10);
    ggfDEBUG_Retry++;
  }
}

void ggfEEPROM_Commit()
{
  if (eeGeaendert) 
  {
    if (SerialDEBUG) Serial.println("EEPROM commit"); 
    EEPROM.put(eeAddress,eeVar);
    EEPROM.commit();
    eeGeaendert=false;
  }
}

void MesseVCC()
{
  pinMode(A0,INPUT);
  intVCC=analogRead(A0);
  delay(100);
  VCC=intVCC/1023.0*4.2*OffsetVccMess;
  if (VCC<lowVCC) 
  {
    Serial.print("VCC: "); Serial.println(VCC);  
    Serial.println("Minimale Akkuspannung unterschritten! ");
    ESP.deepSleep(0); //Schlafen bis Reset oder PowerOn
  }
  hSpannung=int (VCC*100);
  if (SerialDEBUG) 
  {
    Serial.print("VCC: ");
    Serial.print(VCC);  
    Serial.print(" --- (");
    Serial.print(hSpannung);
    Serial.println(" int)");
  }
}

void SetupColdStart()
{
  DEBUG_PRINTLN ("\r\n \r\nPowerOn or Reset");
  ggfDEBUG();
  EEPROM.begin(512);
  eeVar.SerialDEBUG=SerialDEBUG;
  eeVar.hTrocken=hTrockenSoll;
  eeVar.hWartenBisWasser=hWartenBisWasserSoll;
  eeVar.hSchlafZeit=hSchlafZeitSoll;
  eeVar.hPumpZeitSoll=hPumpZeitSoll;
  eeVar.hWartenBisWasserSoll=hWartenBisWasserSoll;
  strcpy(eeVar.hAlias,"Neuer Client!"); 
  eeGeaendert=true;
  if (SerialDEBUG) Serial.println(" \r\n \r\nClientStart- PowerOn oder Reset!");
  if (SerialDEBUG) Serial.println("================================");
  if (SerialDEBUG) 
  {
    Serial.print("hAlias eingestellt: \""); Serial.print(eeVar.hAlias); Serial.println("\"");
    Serial.print("hWartenBisWasserSoll eingestellt: "); Serial.println(eeVar.hWartenBisWasserSoll);
    Serial.print("hWartenBisWasser eingestellt: "); Serial.println(eeVar.hWartenBisWasser); 
    Serial.print("hTrocken eingestellt: "); Serial.println(eeVar.hTrocken); 
    Serial.print("hSchlafZeit eingestellt: "); Serial.println(eeVar.hSchlafZeit); 
    Serial.print("hPumpZeit eingestellt: "); Serial.println(eeVar.hPumpZeitSoll); 
  }
  ggfEEPROM_Commit();
}

void SetupDeepSleep()
{
  DEBUG_PRINTLN ("\r\n \r\nDeepSleepWakeUp");
  EEPROM.begin(512);
  EEPROM.get(eeAddress,eeVar);
  SerialDEBUG=eeVar.SerialDEBUG;
  if (SerialDEBUG) Serial.println(" \r\n \r\nClientStart- Hallo, bin wieder wach!");
  if (SerialDEBUG) Serial.println("====================================");
  if((eeVar.hWartenBisWasserSoll>hWartenBisWasserMax)||(eeVar.hWartenBisWasserSoll<0)) 
  {
    eeVar.hWartenBisWasserSoll=hWartenBisWasserSoll;
    eeGeaendert=true;
  };
  if((eeVar.hWartenBisWasser>hWartenBisWasserMax)||(eeVar.hWartenBisWasser<0)) 
  {
    eeVar.hWartenBisWasser=hWartenBisWasserSoll;
    eeGeaendert=true;
  };
  if((eeVar.hTrocken>hTrockenMax)||(eeVar.hTrocken<60)) 
  {
    eeVar.hTrocken=hTrockenSoll;
    eeGeaendert=true;
  };
  if((eeVar.hSchlafZeit>hSchlafZeitMax)||(eeVar.hSchlafZeit<120)) 
  {
    eeVar.hSchlafZeit=hSchlafZeitSoll;
    eeGeaendert=true;
  };
  if((eeVar.hPumpZeitSoll>hPumpZeitMax)||(eeVar.hPumpZeitSoll<0)) 
  {
    eeVar.hPumpZeitSoll=hPumpZeitSoll;
    eeGeaendert=true;
  };
  if (SerialDEBUG) 
  {
    Serial.print("hAlias vom EEPROM: \""); Serial.print(eeVar.hAlias); Serial.println("\"");
    Serial.print("hWartenBisWasserSoll vom EEPROM: "); Serial.println(eeVar.hWartenBisWasserSoll);
    Serial.print("hWartenBisWasser vom EEPROM: "); Serial.println(eeVar.hWartenBisWasser); 
    Serial.print("hTrocken vom EEPROM: "); Serial.println(eeVar.hTrocken); 
    Serial.print("hSchlafZeit vom EEPROM: "); Serial.println(eeVar.hSchlafZeit); 
    Serial.print("hPumpZeit vom EEPROM: "); Serial.println(eeVar.hPumpZeitSoll); 
  }
}

void SetupErrorStart()
{
  DEBUG_PRINTLN ("\r\n \r\nRestart from Error");
  ggfDEBUG();
  EEPROM.begin(512);
  eeVar.SerialDEBUG=SerialDEBUG;
  eeVar.hTrocken=hTrockenSoll;
  eeVar.hWartenBisWasser=hWartenBisWasserSoll;
  eeVar.hSchlafZeit=hSchlafZeitSoll;
  eeVar.hPumpZeitSoll=hPumpZeitSoll;
  eeVar.hWartenBisWasserSoll=hWartenBisWasserSoll;
  strcpy(eeVar.hAlias,"Neustart FEHLER!"); 
  eeGeaendert=true;
  if (SerialDEBUG) Serial.println(" \r\n \r\nClientStart- Unbekannter Fehler");
  if (SerialDEBUG) Serial.println("================================");
  if (SerialDEBUG) 
  {
    Serial.print("hAlias eingestellt: \""); Serial.print(eeVar.hAlias); Serial.println("\"");
    Serial.print("hWartenBisWasserSoll eingestellt: "); Serial.println(eeVar.hWartenBisWasserSoll);
    Serial.print("hWartenBisWasser eingestellt: "); Serial.println(eeVar.hWartenBisWasser); 
    Serial.print("hTrocken eingestellt: "); Serial.println(eeVar.hTrocken); 
    Serial.print("hSchlafZeit eingestellt: "); Serial.println(eeVar.hSchlafZeit); 
    Serial.print("hPumpZeit eingestellt: "); Serial.println(eeVar.hPumpZeitSoll); 
  }
  ggfEEPROM_Commit();
}

void DS1820Temperatur()
{
  byte i;
//  byte present=0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius;
//  float fahrenheit;
  if (!TemperaturSensor.search(addr)) 
  {
    TemperaturSensor.reset_search();
    delay(250);
    return;
  }
  if (OneWire::crc8(addr,7) !=addr[7]) 
  {
      if  (SerialDEBUG) Serial.println("CRC is not valid!");
      return;
  }
  // the first ROM byte indicates which chip
  switch (addr[0]) 
  {
    case 0x10:
      type_s=1;
      break;
    case 0x28:
      type_s=0;
      break;
    case 0x22:
      type_s=0;
      break;
    default:
      if  (SerialDEBUG) Serial.println("Device is not a DS18x20 family device.");
      return;
  } 
  TemperaturSensor.reset();
  TemperaturSensor.select(addr);
  TemperaturSensor.write(0x44,1);        // start conversion, with parasite power on at the end  
  delay(1000);
//  present=TemperaturSensor.reset();
  TemperaturSensor.reset();
  TemperaturSensor.select(addr);    
  TemperaturSensor.write(0xBE);         // Read Scratchpad
  for (i=0;i<9;i++) 
  {data[i]=TemperaturSensor.read();}
  // Convert the data to actual temperature
  int16_t raw=(data[1]<<8)|data[0];
  if (type_s) 
  {
    raw=raw<<3; // 9 bit resolution default
    if (data[7]==0x10) {raw=(raw&0xFFF0)+12-data[6];}
  } 
  else 
  {
    byte cfg=(data[4]&0x60);
    if (cfg==0x00) raw=raw&~7;  // 9 bit resolution, 93.75 ms
    else if (cfg==0x20) raw=raw&~3; // 10 bit res, 187.5 ms
    else if (cfg==0x40) raw=raw&~1; // 11 bit res, 375 ms
  }
  celsius=(float)raw/16.0;
  hTemperatur=(int)((celsius*10)+.5);
  SensorKennung=(String(addr[1])+String(addr[2])+String(addr[3])+String(addr[4])+String(addr[5])+String(addr[6]));
  if (SerialDEBUG) 
  {
    Serial.print("Sensor: ");
    Serial.print(SensorKennung);
    Serial.print(" --- (");
    Serial.print("Temperatur: ");
    Serial.print(celsius);
    Serial.print(" --- (");
    Serial.print(hTemperatur);
    Serial.println(" int)");
  }
}

void setup()
{
  delay(50);
  rst_info *xyz;
  xyz = ESP.getResetInfoPtr();
  Serial.begin(115200);
  delay(50);
  if((String((*xyz).reason))=="0") 
  {
  //  Serial.println("\r\n \r\nPowerOn");
    DeepSleepWakeUp=1;
  }
  else if((String((*xyz).reason))=="5") 
  {
  //  Serial.println("\r\n \r\nDeepSleepWakeUp");
    DeepSleepWakeUp=0;
  }
  else if((String((*xyz).reason))=="6") 
  {
  //  Serial.println("\r\n \r\nReset");
    DeepSleepWakeUp=1;
  }
  else {DeepSleepWakeUp=-1;} 
  if (DeepSleepWakeUp==0) {SetupDeepSleep();} 
  else if (DeepSleepWakeUp==1) {SetupColdStart();}
  else {SetupErrorStart();}
  if (SerialDEBUG) {digitalWrite(LED,HIGH); pinMode(LED,OUTPUT);}
  MesseVCC();
  DS1820Temperatur();
  WiFi.disconnect();
  WiFi.setAutoConnect(false);
  WiFi.mode(WIFI_OFF); // WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF.
  pinMode(D0, WAKEUP_PULLUP);
  if (isBitTest) {digitalWrite(BitTest,LOW); pinMode(BitTest,OUTPUT);}
  digitalWrite(ChirpSelect,HIGH);
  pinMode(ChirpSelect,OUTPUT);
  pinMode(ChirpDaten,INPUT_PULLUP);  
  pinMode(ChirpClock,INPUT_PULLUP);  
  pinMode(isPumpe,INPUT_PULLUP);  
  pinMode(Pumpe,OUTPUT);
  digitalWrite(Pumpe,LOW); 
  if (SerialDEBUG) 
  {
    digitalWrite(LED,LOW); delay(150); digitalWrite(LED,HIGH); delay(150);                      
    digitalWrite(LED,LOW); delay(150); digitalWrite(LED,HIGH); delay(150);  
  }                    
  // Ab geht es ...
}

void UpDateVomServer()
{
  yield(); 
  int Colon1=LineString.indexOf(';');
  int Colon2=LineString.indexOf(';',Colon1+1);
  int Colon3=LineString.indexOf(';',Colon2+1);
  int Colon4=LineString.indexOf(';',Colon3+1);
  int Colon5=LineString.indexOf(';',Colon4+1);
  int Colon6=LineString.indexOf(';',Colon5+1);
  int Colon7=LineString.indexOf(';',Colon6+1);
  tmp=LineString.substring(Colon1+1,Colon2);
  uTrocken=tmp.toInt();
  tmp=LineString.substring(Colon2+1,Colon3);
  uSchlafZeit=tmp.toInt();
  tmp=LineString.substring(Colon3+1,Colon4);
  uWartenBisWasserSoll=tmp.toInt();
  tmp=LineString.substring(Colon4+1,Colon5);
  uPumpZeitSoll=tmp.toInt();
  tmp=LineString.substring(Colon5+1,Colon6);
  tmp.toCharArray(uAlias,hAliasMaxLength);
  tmp=LineString.substring(Colon6+1,Colon7);
  uExtraSchlafZeit=tmp.toInt();
  if (SerialDEBUG) 
  {
    Serial.print("uAlias: \""); Serial.print(uAlias); Serial.println("\"");
    Serial.print("uTrocken: "); Serial.println(uTrocken);
    Serial.print("uSchlafZeit: "); Serial.println(uSchlafZeit);
    Serial.print("uExtraSchlafZeit: "); Serial.println(uExtraSchlafZeit);
    Serial.print("uWartenBisWasserSoll: "); Serial.println(uWartenBisWasserSoll);
    Serial.print("uPumpZeitSoll: "); Serial.println(uPumpZeitSoll);
  }
  if (eeVar.hTrocken!=uTrocken) 
  {
    eeVar.hTrocken=uTrocken;
    if (SerialDEBUG) {Serial.print("eeVar.hTrocken wurde geändert auf: "); Serial.println(eeVar.hTrocken);}
    eeGeaendert=true;
  }
  if (eeVar.hSchlafZeit!=uSchlafZeit) 
  {
    eeVar.hSchlafZeit=uSchlafZeit;
    if (SerialDEBUG) {Serial.print("eeVar.hSchlafZeit wurde geändert auf: "); Serial.println(eeVar.hSchlafZeit);}
    eeGeaendert=true;
  }
  if (eeVar.hWartenBisWasserSoll!=uWartenBisWasserSoll) 
  {
    eeVar.hWartenBisWasserSoll=uWartenBisWasserSoll;
    if (SerialDEBUG) { Serial.print("eeVar.hWartenBisWasserSoll wurde geändert auf: "); Serial.println(eeVar.hWartenBisWasserSoll);}
    eeGeaendert=true;
  }
  if (eeVar.hWartenBisWasser!=0)
  {
    if (SerialDEBUG) {Serial.print("eeVar.hWartenBisWasser<>0: "); Serial.println(eeVar.hWartenBisWasser);}
    eeGeaendert=true;
  }
  if (eeVar.hPumpZeitSoll!=uPumpZeitSoll) 
  {
    eeVar.hPumpZeitSoll=uPumpZeitSoll;
    if (SerialDEBUG) {Serial.print("eeVar.hPumpZeitSoll wurde geändert auf: "); Serial.println(eeVar.hPumpZeitSoll);}
    eeGeaendert=true;
  }
  if(strcmp(eeVar.hAlias,uAlias)!=0)
  {
    strcpy(eeVar.hAlias,uAlias);
    if (SerialDEBUG) {Serial.print("eeVar.hAlias wurde geändert auf: \""); Serial.print(eeVar.hAlias); Serial.println("\"");}
    eeGeaendert=true;
  };
}

bool ImmerTrue()
{
  yield();
  delayMicroseconds(BitLaenge/6);
  return true;
}

void LeseChirp() 
{
  unsigned long AktuelleUebertragungszeit=0;
  unsigned long StartzeitUebertragung=0;  
  int BitCount=ChirpBitCountMax;
  uint32_t SelectCount=ChirpSelectMax;
  digitalWrite(ChirpSelect,LOW);
  while (ImmerTrue()&&(SelectCount>0)) 
  {
    if (!digitalRead(ChirpClock))
    {
      if (StartzeitUebertragung==0) {StartzeitUebertragung=micros();}
//      if (isBitTest) {digitalWrite(BitTest,HIGH); delayMicroseconds(10); digitalWrite(BitTest,LOW);} // Ping auf Analyser
      Gelesen=Gelesen<<1;
      if (digitalRead(ChirpDaten)) {Gelesen=Gelesen+1;}
       while (!digitalRead(ChirpClock)) {} // Warten bis CLK=H
      BitCount=BitCount-1;
    }
    SelectCount--;
    if (StartzeitUebertragung>0) 
    {
      AktuelleUebertragungszeit=micros();
      if (StartzeitUebertragung+34*BitLaenge<AktuelleUebertragungszeit) 
      {
        Gelesen=20000; 
        SelectCount=0;  
      }
    }
    if (SelectCount==0) {Gelesen=20000;} // wenn Chirp nicht erreicht, Gelesen=20000
    if (BitCount==0) {SelectCount=0;}
 }
  digitalWrite(ChirpSelect,HIGH);
  if (SerialDEBUG) {Serial.print("Gelesen vom Chirp: "); Serial.println(Gelesen);}
}

void DatenVomChirp()
{
    LeseChirp();
    if (Gelesen<20000) 
    {
      hFeuchte=Gelesen;
      hLeseFehler=false;
      if  (SerialDEBUG) {Serial.print("Daten vom Chirp: "); Serial.println(hFeuchte);}     
    }
    else
    {
      hLeseFehler=true;
      if  (SerialDEBUG) Serial.println("Daten vom Chirp: _F_E_H_L_E_R_"); 
    }
}
 
bool MitServerVerbinden()
{
  if (SerialDEBUG) {Serial.println("Mit Server verbinden");}
  WiFi.mode(WIFI_STA); 
  WiFi.begin(server_ssid,server_password);
  int WiFiRetry=0;
  while ((WiFi.status()!=WL_CONNECTED)&&(WiFiRetry<WiFiRetryMax)) 
  {
    delay(100);
    WiFiRetry++;
    if (SerialDEBUG) Serial.print(WiFiRetry%10);
  }
  if(WiFiRetry>=WiFiRetryMax) 
  {
    if (SerialDEBUG) 
    {
      Serial.println();
      Serial.print("Verbindung mit ");
      Serial.print(server_ssid);
      Serial.println(" konnte nicht hergestellt werden! :-( ");
    }
  return false;
  }
  if (SerialDEBUG) 
  {
    Serial.println();
    Serial.print("Verbunden mit ");
    Serial.println(server_ssid);
    Serial.print("IP-Addresse: ");
    Serial.println(WiFi.localIP());
    Serial.print("Server-Adresse: ");
    Serial.println(addr);
    digitalWrite(LED,LOW); delay(50); digitalWrite(LED,HIGH); 
  }
  return true;
}   

void WIFI_Disconnect()   
{
  WiFi.disconnect();
  WiFi.setAutoConnect(false);
  WiFi.mode(WIFI_OFF); // WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF.
  if (SerialDEBUG) {Serial.println("WiFi-Verbingung beendet");}
}

bool DatenZuVomServer()
{
  bool DatenZuVomServerOK=false;
  if (MitServerVerbinden())
  {
    Udp.begin(UdpPort);
    delay(200);               
    Udp.beginPacket(addr,UdpPort);
    Udp.print(SensorKennung);
    Udp.print(";");
    Udp.print(hSpannung);
    Udp.print(";");
    Udp.print(hTemperatur);
    Udp.print(";");
    Udp.print(hWasser);
    Udp.print(";");
    Udp.print(hFeuchte);
    Udp.print(";");
    Udp.print(eeVar.hTrocken);
    Udp.print(";");
    Udp.print(eeVar.hSchlafZeit);
    Udp.print(";");
    Udp.print(eeVar.hWartenBisWasser);
    Udp.print(";");
    Udp.print(eeVar.hWartenBisWasserSoll);
    Udp.print(";");
    Udp.print(eeVar.hPumpZeitSoll);
    Udp.print(";");
    Udp.print(eeVar.hAlias);
    Udp.print(";");
    Udp.endPacket();
    if (SerialDEBUG) 
    {
      Serial.print("Daten zum Server: ");
      Serial.print(SensorKennung);
      Serial.print(";");
      Serial.print(hSpannung);
      Serial.print(";");
      Serial.print(hTemperatur);
      Serial.print(";");
      Serial.print(hWasser);
      Serial.print(";");
      Serial.print(hFeuchte);
      Serial.print(";");
      Serial.print(eeVar.hTrocken);
      Serial.print(";");
      Serial.print(eeVar.hSchlafZeit);
      Serial.print(";");
      Serial.print(eeVar.hWartenBisWasser);
      Serial.print(";");
      Serial.print(eeVar.hWartenBisWasserSoll);
      Serial.print(";");
      Serial.print(eeVar.hPumpZeitSoll);
      Serial.print(";");
      Serial.print(eeVar.hAlias);
      Serial.println(";");
    }
    packetSize=0;
    ServerEchoRetry=0;
    while ((!DatenZuVomServerOK)&&(ServerEchoRetry<ServerEchoRetryMax)) 
    {
      delay(100);
      ServerEchoRetry++;
      packetSize=Udp.parsePacket();
      if (packetSize)
      {
        if (SerialDEBUG) {Serial.printf("UDP-Paket mit %d bytes von %s, port %d empfangen!\n",packetSize,Udp.remoteIP().toString().c_str(),Udp.remotePort());}
        Udp.read(incomingPacket,PacketSizeMax);
        if (SerialDEBUG) Serial.print("Daten vom Server: ");
        if (SerialDEBUG) Serial.println((char *)incomingPacket); 
        LineString=incomingPacket;       
        if (packetSize>3)
        {
          if (SerialDEBUG) Serial.println("UpDate vom Server erhalten");
          UpDateVomServer();
          DatenZuVomServerOK=true;
        }
      }
    }
    if (SerialDEBUG) 
    {
      Serial.print("Wartezeit auf UDP-Echo vom Server: "); Serial.print(ServerEchoRetry*100); Serial.println("ms");
      digitalWrite(LED,LOW); delay(50); digitalWrite(LED,HIGH);
    }
  } 
  WIFI_Disconnect();
  return DatenZuVomServerOK;             
}

void GuteNacht()
{
  pinMode(Pumpe,INPUT);
  if (DatenZuVomServer())
  {
    ggfEEPROM_Commit();
    WachMillis=millis();
    if (SerialDEBUG) 
    {
      Serial.print("ChirpErrorCount: "); 
      Serial.println(ErrorCount); 
      Serial.print("Wachzeit: "); 
      Serial.print(WachMillis);
      Serial.println("ms");
      Serial.print("Schlafzeit: "); 
      Serial.print(eeVar.hSchlafZeit); 
      Serial.println(" Sekunden"); 
      Serial.print("Extra-Schlafzeit: "); 
      Serial.print(uExtraSchlafZeit); 
      Serial.println(" Sekunden"); 
      Serial.println("Lege mich wieder hin, gute Nacht!"); 
      Serial.println(""); 
    }
    ESP.deepSleep(((eeVar.hSchlafZeit+uExtraSchlafZeit)*SchlafZeitMultiplikator)-(WachMillis*1000));
  }
  else 
  {
    ggfEEPROM_Commit();
    WachMillis=millis();
    if (SerialDEBUG) 
    {
      Serial.println("Kein Echo vom Server!");
      Serial.print("ChirpErrorCount: "); 
      Serial.println(ErrorCount); 
      Serial.print("Wachzeit: "); 
      Serial.print(WachMillis);
      Serial.println("ms");
      Serial.print("Schlafzeit: "); 
      Serial.print(eeVar.hSchlafZeit); 
      Serial.println(" Sekunden");
      Serial.print("Server-Error-Extra-Schlafzeit: "); 
      Serial.print(ServerErrorExtraSchlaf); 
      Serial.println(" Sekunden"); 
      Serial.println("Lege mich wieder hin, gute Nacht!"); 
      Serial.println(""); 
    }
    ESP.deepSleep(((eeVar.hSchlafZeit+ServerErrorExtraSchlaf)*SchlafZeitMultiplikator)-(WachMillis*1000));
  }
}

void loop() 
{
        //  zur Einstellung der Pumpenspannung
        //  digitalWrite(Pumpe,HIGH);
        // delay(3000*1000);
  DatenVomChirp();
  if (!hLeseFehler)
  {
    if (eeVar.hWartenBisWasser<0)
    {
      eeVar.hWartenBisWasser=0;
      eeGeaendert=true;
    }
    if (hFeuchte>=eeVar.hTrocken) 
    {
      if (eeVar.hWartenBisWasser>0)
      {
        if (SerialDEBUG) {Serial.print("hWartenBisWasser: "); Serial.print(eeVar.hWartenBisWasser); Serial.println(" --> Kein Wasser!");}
        eeVar.hWartenBisWasser-=eeVar.hSchlafZeit;
        if (eeVar.hWartenBisWasser<0) {eeVar.hWartenBisWasser=0;}
        eeGeaendert=true;
      }
      GuteNacht();
    }
    else
    {
      if (!digitalRead(isPumpe)) 
      {
        if (SerialDEBUG) Serial.println("Keine Pumpe angeschlossen!");
        hWasser=0;
        GuteNacht();
      }
      else
      {
        if (eeVar.hWartenBisWasser>0)
        {
          if (SerialDEBUG) {Serial.print("hWartenBisWasser: "); Serial.print(eeVar.hWartenBisWasser); Serial.println(" --> Kein Wasser!");}
          eeVar.hWartenBisWasser-=eeVar.hSchlafZeit;
          if (eeVar.hWartenBisWasser<0) {eeVar.hWartenBisWasser=0;}
          eeGeaendert=true;
          GuteNacht();
        }
        else
        {
          eeVar.hWartenBisWasser=eeVar.hWartenBisWasserSoll;
          eeGeaendert=true;
          delay(PumpeWarteBisEin*1000);
          digitalWrite(Pumpe,HIGH);
          hWasser=eeVar.hPumpZeitSoll; 
          if (SerialDEBUG) {Serial.println(" --> Pumpe EIN"); Serial.print("Wasser: "); Serial.println(hWasser);}
          delay(eeVar.hPumpZeitSoll*1000);
          digitalWrite(Pumpe,LOW); 
          if (SerialDEBUG) Serial.println(" --> Pumpe AUS");
          GuteNacht();
        }
        
      }  
    }
  }
  else
  {
    delay(500); // Lesefehler vom Chirp ---> Chirp noch mal lesen
    ErrorCount++;
    if (ErrorCount>=ErrorCountMax) 
    {
      hFeuchte=hTrockenMax;
      GuteNacht();
    }
  }
}