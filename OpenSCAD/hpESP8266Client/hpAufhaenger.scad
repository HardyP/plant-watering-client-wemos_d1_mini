DoIt=2; // 0=Alles und Unten zusammen, 1=Unten, 2=Haken
RDA=2.0; // Rundung Aussen
XA=40.0; // X Aussen
YA=40.0; // Y Aussen
ZA=3.0; // Z Aussen
YO=12;
RDO=ZA/2; // Rundung Aussen

// Haken 1
/*
    HB=YO-2; // Haken Breite
HD=1.7; // Haken Dicke
HDU=ZA+0.5; // Haken Durchmesser Unten
HLU=3*HDU; // Haken Laenge Unten
HDO=6.0; // Haken Durchmesser Oben
HLO=2*HDO; // Haken Laenge Oben
*/

// Haken 2

HB=YO-2; // Haken Breite
HD=1.7; // Haken Dicke
HDU=ZA+0.5; // Haken Durchmesser Unten
HLU=3*HDU; // Haken Laenge Unten
HDO=14.0; // Haken Durchmesser Oben
HLO=1.3*HDO; // Haken Laenge Oben


// Haken 3
/*
HB=YO-2; // Haken Breite
HD=1.7; // Haken Dicke
HDU=ZA+0.5; // Haken Durchmesser Unten
HLU=3*HDU; // Haken Laenge Unten
HDO=16.0; // Haken Durchmesser Oben
HLO=2*HDO; // Haken Laenge Oben
*/

// Haken 4
/*
HB=YO-2; // Haken Breite
HD=1.7; // Haken Dicke
HDU=ZA+0.5; // Haken Durchmesser Unten
HLU=3*HDU; // Haken Laenge Unten
HDO=22.0; // Haken Durchmesser Oben
HLO=2*HDO; // Haken Laenge Oben
*/

// Haken 5
/*
HB=YO-2; // Haken Breite
HD=1.7; // Haken Dicke
HDU=ZA+0.5; // Haken Durchmesser Unten
HLU=3*HDU; // Haken Laenge Unten
HDO=26.0; // Haken Durchmesser Oben
HLO=2*HDO; // Haken Laenge Oben

*/

$fn=120;

module Aufhaenger()
{
    intersection()
    {
        union()
        {
            translate([ZA/2,YA/2,ZA/2])  rotate([90,0,0]) cylinder(h=YA,d=ZA,center=false);
            translate([ZA*2.5,-YA/2,0]) cube([XA-ZA*2.5,YA,ZA]);
            translate([ZA/2,-YA/2,0]) cube([ZA*2,(YA-YO*2)/3,ZA]);
            translate([ZA/2,YO+(YA-YO*2)/6,0]) cube([ZA*2,(YA-YO*2)/3,ZA]);
            translate([ZA/2,-(YA-YO*2)/6,0]) cube([ZA*2,(YA-YO*2)/3,ZA]);
        }   
        minkowski()
        {
            translate([RDO,-YA/2+RDO,-ZA+RDO]) cube([XA-RDO*2,YA-RDO*2,ZA*2-RDO*2]);
             sphere(r=RDO);
        }
     }
}

module Ring(Innen,Aussen)
{
    difference()
    {
        cylinder(h=HB,d=Aussen);
        cylinder(h=HB,d=Innen);
    }
}

module Haken()
{
    intersection()
    {
        translate([+HDU/2,-HLU]) Ring(HDU,HDU+HD*2);
        translate([+HDU/2,-HLU-HDU/2-HD,HB/2]) cube([HDU+HD*2,HDU+HD*2,HB],center=true);
    }
    translate([-HD/2,-HLU+HDU/2,HB/2]) cube([HD,HDU,HB],center=true);
    translate([+HDU+HD/2,-HLU+HDU,HB/2]) cube([HD,HDU*2,HB],center=true);
    intersection()
    {
        translate([-HDO/2,HLO/3,0]) Ring(HDO,HDO+HD*2);
        translate([-HDO/2,HLO/3+HDO/2+HD,HB/2]) cube([HDO+HD*2,HDO+HD*2,HB],center=true);
    }
    hull()
    {
        translate([HD/2,HLO-HDO,HB/2]) cylinder(h=HB,d=HD,center=true);
        translate([HD/2,HLO/3,HB/2]) cylinder(h=HB,d=HD,center=true);
     }
    hull()
    {
        translate([-HDO-HD+HD/2,-HLO/3,HB/2]) cylinder(h=HB,d=HD,center=true);
        translate([-HDO-HD+HD/2,HLO/3,HB/2]) cylinder(h=HB,d=HD,center=true);
     }

//    translate([HD/2,HDO/2.8,HB/2]) cube([HD,HDO/6,HB],center=true);
//    translate([-HDO-HD+HD/2,HLO-HDO/4,HB/2]) cube([HD,HDO/2,HB],center=true);
    hull()
    {
        translate([HD/2,HLO-HDO,HB/2]) cylinder(h=HB,d=HD,center=true);
        translate([+HDU+HD/2,-HLU+HDU*2,HB/2]) cylinder(h=HB,d=HD,center=true);
    }
    
}

module Set()
{
         translate([-XA/2,0,]) Aufhaenger();
         translate([50,0,]) Haken();
         translate([-50,0,]) Haken();
}

if (DoIt==0) {Set();} 
else if (DoIt==1) {Aufhaenger();} 
else {Haken();}







   
