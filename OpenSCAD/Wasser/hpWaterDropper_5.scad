$fn=120;

XL=100; // Laenge
WD=1.66; // WandDicke
RDA=2.49; // Rundung Aussen
RDI=RDA/2.2;

SZL=3;  // Stuetze Dicke
SYL=60.0;  // Stuetze Laenge

A1DI=3.1;  // Anschluss 1 Durchmesser Innen
A1DO=5.1;  // Anschluss 1 Durchmesser Aussen
A1L=15.0;  // Anschluss 1 Laenge
A2DI=4.6;  // Anschluss 2 Durchmesser Innen
A2DO=7.1;  // Anschluss 2 Durchmesser Aussen
A2L=16.0;  // Anschluss 2 Laenge

ADI=A1DI;  // Anschluss Durchmesser Innen
ADO=A1DO;  // Anschluss Durchmesser Aussen
AL=A1L;  // Anschluss Laenge
YL=(ADO+WD)*2;  // Breite
ZL=(ADO+WD)*2;  // Hoehe

BD=1.5; // Bohrung Durchmesser

module Spiess()
{
        rotate([90,0,90]) hull()
        {
           translate([0,0,0])  cylinder(h=SZL,d=ADO*2,center=true);
           translate([0,SYL,0])  cylinder(h=0.5,d=2,center=true);
        }
}

module BohrungU()
{
//        translate([XL/2-RDA*3,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
        translate([(XL/2-RDA*3)/2,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
//        translate([0,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
        translate([(-XL/2+RDA*3)/2,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
//        translate([-XL/2+RDA*3,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
}

module BohrungS()
{
        translate([XL/2-RDA*3,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
//        translate([(XL/2-RDA*3)/2,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
        translate([0,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
//        translate([(-XL/2+RDA*3)/2,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
        translate([-XL/2+RDA*3,0,ZL/2]) color("Red",1.0) cylinder(h=YL,d=BD,center=true);
}

module DoIt() 
{
    difference() 
    {
        union()
        {
            translate([-XL/2+RDA*6,0,0]) color("LightGreen",0.5) Spiess();
            translate([XL/2-RDA*6,0,0]) color("LightGreen",0.5) Spiess();
            color("LightGreen",0.5) hull()
            {
                translate([-XL/2+RDA,-YL/2+RDA,-ZL/2+RDA]) sphere(RDA);
                translate([XL/2-RDA,-YL/2+RDA,-ZL/2+RDA]) sphere(RDA);
                translate([-XL/2+RDA,+YL/2-RDA,-ZL/2+RDA]) sphere(RDA);
                translate([XL/2-RDA,+YL/2-RDA,-ZL/2+RDA]) sphere(RDA);
                
                translate([-XL/2+RDA,-YL/2+RDA,ZL/6-RDA]) sphere(RDA);
                translate([XL/2-RDA,-YL/2+RDA,ZL/6-RDA]) sphere(RDA);
                translate([-XL/2+RDA,+YL/2-RDA,ZL/6-RDA]) sphere(RDA);
                translate([XL/2-RDA,+YL/2-RDA,ZL/6-RDA]) sphere(RDA);
                
                translate([-XL/2+RDA,-RDA/2,ZL/2-RDA]) sphere(RDA);
                translate([XL/2-RDA,-RDA/2,ZL/2-RDA]) sphere(RDA);
                translate([-XL/2+RDA,RDA/2,ZL/2-RDA]) sphere(RDA);
                translate([XL/2-RDA,RDA/2,ZL/2-RDA]) sphere(RDA);
            }
            translate([XL/2,0,0])   rotate([0,90,0]) cylinder(h=AL,d=ADO,center=false);
            translate([XL/2-1,0,0])   rotate([0,90,0]) cylinder(h=AL/3,d1=ADO*1.8,d2=ADO,center=false);
         }
        union()
        {
           translate([0,0,0]) color("Red",1.0) hull()
            {
                translate([-XL/2+RDI+WD,-YL/2+RDI+WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([XL/2-RDI-WD,-YL/2+RDI+WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([-XL/2+RDI+WD,+YL/2-RDI-WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([XL/2-RDI-WD,+YL/2-RDI-WD,-ZL/2+RDI+WD]) sphere(RDI);
                
                translate([-XL/2+RDI+WD,-YL/2+RDI+WD,ZL/5-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,-YL/2+RDI+WD,ZL/5-RDI-WD]) sphere(RDI);
                translate([-XL/2+RDI+WD,+YL/2-RDI-WD,ZL/5-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,+YL/2-RDI-WD,ZL/5-RDI-WD]) sphere(RDI);
                
                translate([-XL/2+RDI+WD,-RDI/2+WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,-RDI/2+WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([-XL/2+RDI+WD,RDI/2-WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,RDI/2-WD,ZL/2-RDI-WD]) sphere(RDI);
            }
          translate([XL/2-WD,0,0])  color("Red",1.0)  rotate([0,90,0]) cylinder(h=AL/3,d1=ADI*2,d2=ADI,center=false);
          translate([XL/2-WD,0,0]) color("Red",1.0)   rotate([0,90,0]) cylinder(h=AL*1.2,d=ADI,center=false);
            BohrungU();
            translate([0,0,0]) rotate([50,0,0]) BohrungS();
            translate([0,0,0]) rotate([-50,0,0]) BohrungS();
      }
    }
}

DoIt();