// Abwandlung von http://www.thingiverse.com/thing:1695285
// Customizable Hammond Style Box by stylesuxx 
// http://creativecommons.org/licenses/by-nc/3.0
use <hpAufhaenger.scad>

/* [Hidden] */
//$fn = 36*2;
$fn = 120;
print = true;

/* [Type] */
// Which part of the enclosure to view?
part = "body"; // [both, body, lid, test]

// Which type of box do you want?
type = "VonInnen"; // [VonInnen, VonAussen]

// Abmessungen
Breite = 50; 
Laenge = 90;
Hoehe = 35;
DeckelHoehe = 10;
WandDicke = 2;
snapWidth = 1.00; // Snap im Deckel
outerDiameter = 6;

// fuer magnetisch 

verschluss = "m";
postDiameter = 0;
postWidthBody = 6.5;
postWidthLid = 6.5;
holeWidthBody = 4.5;
holeHeightBody = 4.2;
holeWidthLid = 4.3;
holeHeightLid = 4.2;
xOffsetHole = 2.5;
yOffsetHole = 2.5;
zOffsetHole = 0.0;
zOffsetPost = -0.3;

// ende magnetisch

// fuer schrauben
/*
verschluss = "s";
postDiameter = 0;
postWidthBody = 7;
postWidthLid = 7;
holeDiameterBody = 2.2;
holeDiameterLid = 3.2;
holeHeadDiameterLid = 6.5;
holeHeadHeight=2.5;
xOffsetHole = 6;
yOffsetHole = 6;
zOffsetHole = 0;
zOffsetPost = -0.3;
*/
// ende schrauben

enclosure(type, part);
 
module enclosure(type, part, boxColor) {
  if(type == "VonInnen")
    VonInnen(part, boxColor);
  if(type == "VonAussen")
    VonAussen(part, boxColor);
}

module VonInnen(part="both", boxColor="blue") 
  {
  outerLength = Laenge+WandDicke*2;
  outerWidth = Breite+WandDicke*2;
  outerHeight = Hoehe+WandDicke*2;
  lidHeight = DeckelHoehe;

  innerLength = Laenge;
  innerWidth = Breite;
  innerHeight = Hoehe;

  hammond(
    outerLength,
    outerWidth,
    outerHeight,
    lidHeight,
    innerLength,
    innerWidth,
    innerHeight,
    part, 
    boxColor
    );
  }

module VonAussen(part="both", boxColor="blue") 
  {
  outerLength = Laenge;
  outerWidth = Breite;
  outerHeight = Hoehe;
  lidHeight = DeckelHoehe;

  innerLength = Laenge-WandDicke*2;
  innerWidth = Breite-WandDicke*2;
  innerHeight = Hoehe-WandDicke*2;

  hammond(
    outerLength,
    outerWidth,
    outerHeight,
    lidHeight,
    innerLength,
    innerWidth,
    innerHeight,
    part, 
    boxColor
    );
  }

module hammond(
    outerLength,
    outerWidth,
    outerHeight,
    lidHeight,
    innerLength,
    innerWidth,
    innerHeight,
    part, 
    boxColor 
)
{
  postRadius = postDiameter / 2;
  outerRadius = outerDiameter / 2;
  bodyHeight = outerHeight - lidHeight;

  xOffset = (outerLength - innerLength) / 2;
  yOffset = (outerWidth - innerWidth) / 2;
  zOffset = (outerHeight - innerHeight) / 2;

  screwPosts = [
    [0, 0, zOffset],
    [outerLength, 0, zOffset],
    [outerLength, outerWidth, zOffset],
    [0, outerWidth, zOffset]
  ];

  translate([outerWidth / 2, -outerHeight / 2, outerLength]) {
    rotate([0,90,90]) {
      if(part == "both" && !print) {
        translate([outerLength, 0, outerHeight])
          rotate([0,180,0])
            color(boxColor)
              renderBody();

        %renderLid();
      }
      
      if(part == "both" && print) {
        yOffset = (outerLength + outerWidth) / 2;
        pos = [outerLength, yOffset, 0];
        rot = [90, 0, 270];

        translate(pos)
          rotate(rot){
            renderBody();
            
            translate([0, outerWidth +3, 0])
              renderLid();
          }
      }

      if(part == "body") {
        yOffset = (outerLength + outerWidth) / 2;
        pos = (print) ? [outerLength, yOffset, 0] : [outerLength, 0, outerHeight];
        rot = (print) ? [90, 0, 270] : [0,180,0];

        translate(pos)
          rotate(rot)
            color(boxColor)
              renderBody();
      }

      if(part == "lid") {
        yOffset = (outerLength + outerWidth) / 2;
        pos = (print) ? [outerLength, yOffset, 0] : [0, 0, 0];
        rot = (print) ? [90, 0, 270] : [0, 0, 0];

        translate(pos)
          rotate(rot)
            color(boxColor)
              renderLid();
      }

      if(part == "test") {
        length = 13;
        width = 13;
        height = 30;

        translate([outerLength - outerHeight, 0, outerLength/4])
        rotate([0, 90, 0]) {
          intersection() {
            translate([0, 0, outerHeight - height])
              cube([length, width, height]);

            translate([outerLength, 0, outerHeight])
              rotate([0,180,0])
                  renderBody();
          }

          translate([0, -outerWidth +  (length * 2), 0])
            intersection() {
              translate([0, outerWidth - width, outerHeight - height])
                cube([length, width, height]);
              
              translate([outerLength, 0, outerHeight])
                rotate([0,180,0])
                  renderLid();
            }
        }
      }
    }
  }
  
  module renderBody() {
   postHeight = bodyHeight-zOffset+zOffsetPost;
   // Hier moegliche Ausschnitte im Body-Boden testen       
   // translate([30, 18, 0]) cube([5,5,10],true);
   //  
   difference() {
      difference() {
        roundBody(bodyHeight);
        difference() {
          translate([xOffset, yOffset, zOffset])
            cube([innerLength, innerWidth, bodyHeight]);

          screwPosts(postWidthBody, postHeight);
        }
      }
    // Hier moegliche Ausschnitte im Body-Boden         
    // translate([30, 18, 0]) cube([5,5,10],true);
    // translate([50, 18, 0]) cylinder(d=5.5, h=10);
    //

    // Belueftungsloecher
    translate([WandDicke+Laenge-16,Breite/2+WandDicke-14,WandDicke+10]) rotate([0,90,0]) cylinder(d=1.5, h=30);
     translate([WandDicke+Laenge-16,Breite/2+WandDicke-7,WandDicke+10]) rotate([0,90,0]) cylinder(d=1.5, h=30);
     translate([WandDicke+Laenge-16,Breite/2+WandDicke,WandDicke+10]) rotate([0,90,0]) cylinder(d=1.5, h=30);
     translate([WandDicke+Laenge-16,Breite/2+WandDicke+7,WandDicke+10]) rotate([0,90,0]) cylinder(d=1.5, h=30);
     translate([WandDicke+Laenge-16,Breite/2+WandDicke+14,WandDicke+10]) rotate([0,90,0]) cylinder(d=1.5, h=30);
     translate([WandDicke+12,Breite/2+WandDicke,(Hoehe-DeckelHoehe+WandDicke)/2+WandDicke+7]) rotate([0,90,90]) cylinder(d=1.5, h=60,center=true);
     translate([WandDicke+12,Breite/2+WandDicke,(Hoehe-DeckelHoehe+WandDicke)/2+WandDicke]) rotate([0,90,90]) cylinder(d=1.5, h=60,center=true);
     translate([WandDicke+12,Breite/2+WandDicke,(Hoehe-DeckelHoehe+WandDicke)/2+WandDicke-7]) rotate([0,90,90]) cylinder(d=1.5, h=60,center=true);

      if(verschluss == "m") 
      {   
        mpostHoles(holeHeightBody,holeWidthBody,postHeight-holeHeightBody+zOffsetHole);
      }
      if(verschluss == "s") 
      {   
        holeRadiusBody = holeDiameterBody / 2;
        spostHoles(outerHeight-lidHeight, holeRadiusBody, zOffsetHole);     
      }
     }
     // Hier moegliche Posts fuer Platinen etc im Body-Boden einfuegen
     // xy ab Innenkante
 //    holePost(x=20,y=20,height=6,diaout=5,diahole=2.5);
//     holePost(x=20+30,y=20,height=6,diaout=5,diahole=2.5);
     
      //Fuehrung Platine
    translate([WandDicke,6+WandDicke,WandDicke]) cube([8.5,2,26.7]);
    translate([Laenge-2.5-WandDicke-WandDicke,6+WandDicke,WandDicke]) cube([8.5,2,26.7]);
//    translate([WandDicke,6+WandDicke,WandDicke]) cube([16,2,3]);
//    translate([WandDicke+Laenge-16,6+WandDicke,WandDicke]) cube([16,2,3]);
    translate([5+WandDicke,WandDicke,WandDicke]) cube([4,3.5,26.7]);
    translate([Laenge-3-WandDicke-WandDicke,WandDicke,WandDicke]) cube([4,3.5,26.7]);
//    translate([6+WandDicke,WandDicke,WandDicke]) cube([10,3.5,3]);
//    translate([Laenge-14,WandDicke,WandDicke]) cube([10,3.5,3]);

      
  }
  
  module renderLid() 
  {
    zOffsetPost = 0.1;
    postHeight = lidHeight-zOffset+zOffsetPost;
    // Hier moegliche Ausschnitte im Deckel testen       
    //translate([0, 0, 0]) cube([5,5,10],true);
    //translate([(innerLength/2),25, 0]) cube([62,21,10],true);
    //  
    difference() 
    {
      group() 
      {
        difference() 
        {
           roundBody(lidHeight);
          translate([xOffset, yOffset, zOffset])
            cube([innerLength, innerWidth, lidHeight]);
        }
        translate([0, 0, -zOffsetPost]) screwPosts(postWidthLid, postHeight);       
        translate([(outerLength-innerLength)/2+postWidthBody, yOffset, zOffset])
          cube([innerLength-postWidthBody-postWidthBody, snapWidth, lidHeight - zOffset + 1.5]);
        translate([(outerLength-innerLength)/2+postWidthBody, outerWidth - yOffset - snapWidth, zOffset])
          cube([innerLength-postWidthBody-postWidthBody, snapWidth, lidHeight - zOffset + 1.5]);
        translate([yOffset, (outerWidth-innerWidth)/2+postWidthBody, zOffset])
          cube([snapWidth, innerWidth-postWidthBody-postWidthBody, lidHeight - zOffset + 1.5]);
        translate([outerLength - yOffset - snapWidth, (outerWidth-innerWidth)/2+postWidthBody, zOffset])
          cube([snapWidth, innerWidth-postWidthBody-postWidthBody, lidHeight - zOffset + 1.5]);
      }
      // Hier moegliche Ausschnitte im Deckel         
      //translate([30, 18, 0]) cube([5,5,10],true);
      // translate([50, 18, 0]) cylinder(d=5.5, h=10);
     // translate([(innerLength/2)-(61.8/2),30, 0]) cube([61.8,25.2,10],false);

       translate([0,Breite/2-2,DeckelHoehe]) cube([10,7.5,3.5],true); // Kabeldurchbruch Chirp
       translate([0,Breite/2+10,DeckelHoehe]) cube([10,3.0,3.5],true); // kabeldurchbruch Pumpe
       translate([0,Breite/2+16,zOffset+3]) cube([10,7.8,2.8],true); // kabeldurchbruch Power

      //
      if(verschluss == "m") 
      {   
        mpostHoles(holeHeightLid,holeWidthLid,postHeight-holeHeightLid+zOffsetHole);
      }
      if(verschluss == "s") 
      {   
        holeHeadRadius = holeHeadDiameterLid/2;
        holeRadiusLid = holeDiameterLid / 2;
        spostHoles(outerHeight, holeRadiusLid, zOffsetHole-WandDicke+holeHeadHeight/2);     
        postHolesSenkung(holeHeadHeight, holeHeadRadius, holeRadiusLid,  zOffsetHole-WandDicke+holeHeadHeight/2);  
      }
    }
      // Hier moegliche Posts fuer Platinen etc einfuegen
     // xy ab Innenkante
    // holePost(x=(innerLength/2)-(82/2),y=30-16,height=10.5,diaout=5,diahole=2.5);
     //holePost(x=(innerLength/2)-(82/2)+82,y=30-16,height=10.5,diaout=5,diahole=2.5);
     //holePost(x=(innerLength/2)-(82/2),y=30-16+55,height=10.5,diaout=5,diahole=2.5);
     //holePost(x=(innerLength/2)-(82/2)+82,y=30-16+55,height=10.5,diaout=5,diahole=2.5);
     //holePost(x=20+30,y=20,height=6,diaout=5,diahole=2.5);

    //Fuehrung Platine
    translate([1+WandDicke,6+WandDicke,0]) cube([9,2,10]);
    translate([Laenge-4-WandDicke-WandDicke,6+WandDicke,0]) cube([9,2,10]);
//    translate([1+WandDicke,6+WandDicke,0]) cube([Laenge-10,2,5]);
    translate([6+WandDicke,1+WandDicke,0]) cube([4,2.5,10]);
    translate([Laenge-4-WandDicke-WandDicke,1+WandDicke,0]) cube([4,2.5,10]);
//    translate([6+WandDicke,1+WandDicke,0]) cube([10,2.5,5]);
//    translate([Laenge-14,1+WandDicke,0]) cube([10,2.5,5]);

     // Aufhaenger
    translate([Laenge+18,Breite/2+WandDicke,0]) rotate([0,0,180]) difference() 
    {
        Aufhaenger();
        translate([15,-22,2])cube([40,44,4]);
    }

    // Halterung Akku
 //       color("lightblue") translate([(Laenge-70)/2+WandDicke,Breite-10-2,10+WandDicke+5])  rotate([90,0,90]) cylinder(d=19, h=70);  
        difference() 
        {
            translate([(Laenge-40)/2+WandDicke,Breite-20-2,0]) cube([40,20,14]);
            union()
            {
                color("red") translate([(Laenge-70)/2+WandDicke,Breite-10-2,10+WandDicke+5])  rotate([90,0,90]) cylinder(d=19, h=70);  
                difference() 
                {
                    color("red") translate([(Laenge-10)/2+WandDicke,Breite-10-2,10+WandDicke+5])  rotate([90,0,90]) cylinder(d=28, h=10); 
                    color("red") translate([(Laenge-10)/2+WandDicke,Breite-10-2,10+WandDicke+5])  rotate([90,0,90]) cylinder(d=23, h=10); 
                }
            }
        }
//
    }

  module roundBody(cutHeight) {
    cornersBottom = [
      [outerRadius, outerRadius, outerRadius],
      [outerLength - outerRadius, outerRadius, outerRadius],
      [outerLength - outerRadius, outerWidth - outerRadius, outerRadius],
      [outerRadius, outerWidth - outerRadius, outerRadius]
    ];

    cornersTop = [
      [outerRadius, outerRadius, outerRadius],
      [outerLength - outerRadius, outerRadius, outerRadius],
      [outerLength - outerRadius, outerWidth - outerRadius, outerRadius],
      [outerRadius, outerWidth - outerRadius, outerRadius]
    ];

    bodyInnerWidth = outerWidth - outerRadius * 2;
    bodyInnerLength = outerLength - outerRadius * 2;
    bodyInnerHeight = cutHeight- outerRadius;

    translate([0, outerRadius,outerRadius])
      cube([outerLength, bodyInnerWidth, bodyInnerHeight]);
    
    translate([outerRadius, 0, outerRadius])
      cube([bodyInnerLength, outerWidth,  bodyInnerHeight]);

    for(pos = cornersTop)
      translate(pos)
        cylinder(r=outerRadius, h = bodyInnerHeight);

    //1832 // 7170
    difference() {
      hull() {
        for(pos = cornersBottom)
          translate(pos)
            sphere(r=outerRadius);
      }

      translate([-1, -1, cutHeight])
        cube([outerLength + 2, outerWidth + 2, cutHeight]);
    }
  }

  module mpostHoles(height, width, zOffset) 
  {
    for(i = [0:3])
      translate(screwPosts[i])
      rotate([0, 0, 90 * i])
//      translate([xOffsetHole, yOffsetHole, zOffset])  cube([width,width,height],false);
//   translate([xOffsetHole+width/2, yOffsetHole+width/2, zOffset])  cylinder(h=height,d=width,center=false);
    translate([xOffsetHole+width/2, yOffsetHole+width/2, zOffset-(5*0.12)])  cylinder(h=height,d=width,center=false);
   }


  module spostHoles(height, radius, zOffset) 
  {
//echo(height=height,radius=radius,zOffset=zOffset);  
    for(i = [0:3])
      translate(screwPosts[i])
      rotate([0, 0, 90 * i])
      translate([xOffsetHole, yOffsetHole, zOffset])
      cylinder(r=radius, h=height);
  }
  
  module postHolesSenkung(height, radius1, radius2, zOffset) 
  {
//echo(height=height,radius1=radius1,radius2=radius2,zOffset=zOffset);  
    for(i = [0:3])
      translate(screwPosts[i])
      rotate([0, 0, 90 * i])
//      translate([xOffsetHole, yOffsetHole, -0.41])
      translate([xOffsetHole, yOffsetHole, zOffset])
      cylinder(h=height, r1=radius1, r2=radius2, center=true);
 }

   module holePost(x,y,height,diaout,diahole) 
   {
      translate([x+WandDicke,y+WandDicke,height/2+WandDicke])
      {
        difference() 
        {
           cylinder(r=diaout/2, h=height,center=true);
           cylinder(r=diahole/2, h=height,center=true);
        }
      }
   } 
  
  module screwPosts(postWidth, height) {
    for(i = [0:3])
      translate(screwPosts[i])
      rotate([0, 0, 90 * i])
      screwPost(postWidth, height);
  }

  module screwPost(postWidth, height) {
    offsetRadius = postWidth - postRadius;
    translate([min(xOffset, yOffset), min(xOffset, yOffset), 0]) {
      cube([postWidth, postWidth - postRadius, height]);
      cube([postWidth - postRadius, postWidth , height]);
      translate([offsetRadius, offsetRadius, 0])
        cylinder(r=postRadius, h=height);
    }
  }
}