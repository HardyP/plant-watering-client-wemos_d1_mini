$fn=120;

Anzahl=5;
YL1=3;  // Breite
A1DI=3.5; // Anschluss 1 Durchmesser Innen
A1DO=5.0; // Anschluss 1 Durchmesser Aussen
A1L=15.0; // Anschluss 1 Laenge
Bohr=2.0;
RDA=1.0;

module DoIt() 
{
    difference()
    {
        union()
        {
            cylinder(h=YL1,d=A1DO*1.5,center=false);
            translate([0,0,YL1])  cylinder(h=A1L,d1=A1DO,d2=A1DI,center=false);
            // rotate([0,0,90])  translate([0,-YL1/2,0])   rotate([90,0,180]) cylinder(h=YL1,d=A2DO*1.5,center=false);
        }
        translate([0,0,0])  cylinder(h=A1L+YL1,d=Bohr,center=false);
    }
}

//DoIt();
for (i=[0:Anzahl-1]) translate ([i*A1DO*2.5,0,0]) DoIt();