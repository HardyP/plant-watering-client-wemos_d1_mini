$fn=360;

YL1=3;  // Breite
A1DI=3.3; // Anschluss 1 Durchmesser Innen
A1DO=5.1; // Anschluss 1 Durchmesser Aussen
A1L=10.0; // Anschluss 1 Laenge
A2DI=4.6; // Anschluss 1 Durchmesser Innen
A2DO=7.1; // Anschluss 1 Durchmesser Aussen
A2L=10.0; // Anschluss 1 Laenge
RDA=1.0;

module DoIt() 
{
   difference() 
    {
        union()
        {
           rotate([0,0,90])  translate([0,-YL1/2,0])   rotate([90,0,0]) cylinder(h=A2L,d=A2DO,center=false);
           rotate([0,0,90])  translate([0,-YL1/2,0])   rotate([90,0,0]) cylinder(h=A2L/6,d1=A2DO*1.2,d2=A2DO,center=false);
           rotate([0,0,90])  translate([0,+YL1/2,0])   rotate([90,0,180]) cylinder(h=A1L,d=A1DO,center=false);
           rotate([0,0,90])  translate([0,+YL1/2,0])   rotate([90,0,180]) cylinder(h=A1L/6,d1=A2DO*1.2,d2=A1DO,center=false);
            rotate([0,0,90])  translate([0,-YL1/2,0])   rotate([90,0,180]) cylinder(h=YL1,d=A2DO*1.2,center=false);
        }
        union()
        {
           rotate([0,0,90])  translate([0,0,0])   rotate([90,0,0]) cylinder(h=A2L+YL1,d=A2DI,center=false);
           rotate([0,0,90])  translate([0,0,0])   rotate([90,0,180]) cylinder(h=A1L+YL1,d=A1DI,center=false);
           rotate([0,0,90])  translate([0,0,0])   rotate([90,0,180]) cylinder(h=YL1,d2=A1DI,d1=A2DI,center=false);
        }
    }
}
//DoIt();
rotate([0,90,0]) DoIt();