$fn=120;

XL=16; // Laenge
YL1=16;  // Breite
YL2=16;  // Breite oben
ZL=16; // Hoehe
WD=2.49; // WandDicke
RDA=2.49;
RDI=0.1;

A1DI=3.1; // Anschluss 1 Durchmesser Innen
A1DO=5.1; // Anschluss 1 Durchmesser Aussen
A1L=15.0; // Anschluss 1 Laenge
A2DI=4.6; // Anschluss 1 Durchmesser Innen
A2DO=7.1; // Anschluss 1 Durchmesser Aussen
A2L=15.0; // Anschluss 1 Laenge

StabL=120.0; // Stablänge - Rod length
StabB=8.0; // Stabbreite - Rod width
StabRD=2.0; // Stabrundung - Rod rounding
SpitzeL=70.0; // Länge der Spitze - length of tip
SpitzeB=15.0; // Breite der Spitze - width of tip


module DoIt() 
{
    difference() 
    {
        union()
        {
            color("LightGreen",0.5) hull()
            {
                translate([-XL/2+RDA,-YL1/2+RDA,-ZL/2+RDA]) sphere(RDA);
                translate([XL/2-RDA,-YL1/2+RDA,-ZL/2+RDA]) sphere(RDA);
                translate([-XL/2+RDA,+YL1/2-RDA,-ZL/2+RDA]) sphere(RDA);
                translate([XL/2-RDA,+YL1/2-RDA,-ZL/2+RDA]) sphere(RDA);
                
                translate([-XL/2+RDA,-YL2/2+RDA,ZL/2-RDA]) sphere(RDA);
                translate([XL/2-RDA,-YL2/2+RDA,ZL/2-RDA]) sphere(RDA);
                translate([-XL/2+RDA,+YL2/2-RDA,ZL/2-RDA]) sphere(RDA);
                translate([XL/2-RDA,+YL2/2-RDA,ZL/2-RDA]) sphere(RDA);
            }
           translate([0,-YL1/2+RDA,0])   rotate([90,0,0]) cylinder(h=A1L,d=A1DO,center=false);
           translate([0,-YL1/2+RDA-1,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2,d2=A1DO,center=false);
           rotate([0,0,180]) 
           {
                translate([0,-YL1/2+RDA,0])   rotate([90,0,0]) cylinder(h=A1L,d=A1DO,center=false);
                translate([0,-YL1/2+RDA-1,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2,d2=A1DO,center=false);
           }
                translate([YL1/4+RDA,0,0])   rotate([90,0,90]) cylinder(h=A2L,d=A2DO,center=false);
                translate([YL1/4+RDA+0.5,0,0])   rotate([90,0,90]) cylinder(h=A2L/3,d1=A2DO*2,d2=A2DO,center=false);
      }
         union()
        {
             translate([0,0,0]) color("Red",1.0) hull()
            {
                translate([-XL/2+RDI+WD,-YL1/2+RDI+WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([XL/2-RDI-WD,-YL1/2+RDI+WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([-XL/2+RDI+WD,+YL1/2-RDI-WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([XL/2-RDI-WD,+YL1/2-RDI-WD,-ZL/2+RDI+WD]) sphere(RDI);
                
                translate([-XL/2+RDI+WD,-YL2/2+RDI+WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,-YL2/2+RDI+WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([-XL/2+RDI+WD,+YL2/2-RDI-WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,+YL2/2-RDI-WD,ZL/2-RDI-WD]) sphere(RDI);
            }
           translate([0,-YL1/2+RDA-1,0])   rotate([90,0,0]) color("Red",1.0) cylinder(h=A1L*2,d=A1DI,center=true);
           rotate([0,0,180]) translate([0,-YL1/2+RDA-1,0])   rotate([90,0,0]) color("Red",1.0) cylinder(h=A1L*2,d=A1DI,center=true);
          translate([YL1/4+RDA+3,0,0])   rotate([90,0,90]) color("Red",1.0) cylinder(h=A2L*2,d=A2DI,center=true);
           translate([0,-YL1/2+RDA+WD,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2-2.0,d2=A1DO-2.0,center=false);
           rotate([0,0,180]) translate([0,-YL1/2+RDA+WD,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2-2.0,d2=A1DO-2.0,center=false);
           translate([YL1/4+RDA-WD+1.3,0,0])   rotate([90,0,90]) cylinder(h=4.0,d1=A2DO+2.4,d2=A2DO-3,center=false);
      }
   }
}

module StabSpitze()
{
    linear_extrude(height=StabL-SpitzeL) translate ([0,0,0])
    {
        offset (StabRD)
        offset (-StabRD)
        square ([StabB, StabB], center = true);
    }
   hull()
   {
     translate ([-SpitzeB,-StabB/2+StabRD/4,StabL-SpitzeL+10])   rotate([90,0,0]) cylinder(h=StabRD/2,d=StabRD,center=true);  
     translate ([SpitzeB,-StabB/2+StabRD/4,StabL-SpitzeL+10])   rotate([90,0,0]) cylinder(h=StabRD/2,d=StabRD,center=true);  
     translate ([-StabB/2+StabRD,StabB/2-StabRD/2,StabL-SpitzeL])   rotate([90,0,0]) cylinder(h=StabRD,d=StabRD,center=true);  
     translate ([+StabB/2-StabRD,StabB/2-StabRD/2,StabL-SpitzeL])   rotate([90,0,0]) cylinder(h=StabRD,d=StabRD,center=true);  
     translate ([-StabB/2+StabRD,-StabB/2+StabRD/2,StabL-SpitzeL])   rotate([90,0,0]) cylinder(h=StabRD,d=StabRD,center=true);  
     translate ([+StabB/2-StabRD,-StabB/2+StabRD/2,StabL-SpitzeL])   rotate([90,0,0]) cylinder(h=StabRD,d=StabRD,center=true);  
     translate ([0,-StabB/2+StabRD/4,StabL])   rotate([90,0,0]) cylinder(h=StabRD/2,d=StabRD/2,center=true);  
   } 
   hull()
   {
        translate ([-StabB/2+StabRD,StabB/2-StabRD/2,StabL-SpitzeL])   rotate([90,0,0]) cylinder(h=StabRD,d=StabRD,center=true);  
        translate ([+StabB/2-StabRD,StabB/2-StabRD/2,StabL-SpitzeL])   rotate([90,0,0]) cylinder(h=StabRD,d=StabRD,center=true);  
        translate ([0,StabB/2+SpitzeB,StabL-SpitzeL+10])   rotate([90,0,0]) cylinder(h=StabRD/2,d=StabRD/2,center=true);  
        translate ([0,-StabB/2+StabRD/4,StabL])   rotate([90,0,0]) cylinder(h=StabRD/2,d=StabRD/2,center=true);  
   }
}    

DoIt();
rotate([0,0,90]) translate ([0,0,ZL/2]) StabSpitze();