# plant-watering-client-wemos_d1_mini

Client for plant watering with esp8266 _ Client zur Pflanzenbewässerung mit esp8266

<img src="/20200221-170606.png" alt="TitelBild"/>

Auf meiner Terrasse züchte ich Tomaten, Paprika und anderes Grünzeug in Töpfen.
Durch die letzten heißen Sommer war ich sehr verunsichert über die Menge der Wassergabe, war ständig hin und her gerissen zwischen zu viel und zu wenig Wasser.
So habe ich mir 2019 Gedanken über ein eigenes Bewässerungssystem für meine Zwecke zu machen. Nach vielen Experimenten habe ich ein passendes
System entwickelt, und konnte schon im vergangenen Jahr dieses System 3 Monate lang an meinen Bohnen testen.
Dieses Jahr, 2020, ist geplant alle Pflanzen komplett automatisch zu bewässern.

Die detailierte Beschreibung befindet sich in der LiesMich.pdf
Dieses Projekt ist ebenfalls auf https://www.thingiverse.com/thing:4239726 zu finden.
Der zugehörige Pflanzen-Server ist unter https://gitlab.com/HardyP/plant-watering-server-wemos-lolin32 zu finden.

Bitte betrachte das hier geschriebene nicht als sichere Nachbauanleitung, sondern als Anregung für ein eigenes Projekt!

-----------------

On my terrace I grow tomatoes, peppers and other greenery in pots.
Because of the last hot summers, I was very uncertain about the amount of water to give, was constantly torn between too much and too little water.
So in 2019 I have to think about an own irrigation system for my purposes. After many experiments I have found a suitable
system, and was able to test this system on my beans for 3 months last year.
This year, 2020, it is planned to irrigate all plants completely automatically.

The detailed description can be found in the readme.pdf
This project can also be found at https://www.thingiverse.com/thing:4239726.
The corresponding plant server can be found at https://gitlab.com/HardyP/plant-watering-server-wemos-lolin32 .

Please don't consider the one written here as a safe reproduction guide, but as a suggestion for your own project!

-------------------
<p>  </p>

===> 29.06.2021-15:00 <===

OpenSCAD - Client-Gehäuse ohne Akku, Änderungen

===> 20.04.2021-19:20 <===

OpenSCAD - Client-Gehäuse ohne Akku hochgeladen

===> 19.06.2020-13:26 <===

OpenSCAD - Bewässerungsring (hpPlantWateringRing) hochgeladen

===> 18.05.2020-15:35 <===

Die neuen Platinen mit dem geänderten Layout sind eingetroffen, getestet und für gut befunden.

<img src="/pics/20200518-151912.jpg" alt="Neue Platine"/>
<p>  </p>

===> 17.05.2020-13:00 <===

Notwendige Anpassungen für den Betrieb des Pflanzen-Servers als Access Point (AP) durchgeführt. Der Betrieb des Pflanzen-Servers als Access Point (AP) wird mit "#define UseSoftAP" in "setup_daten.h" aktiviert. In "ssid_password.h" werden die entsprechenden Netzwerkzugänge eingestellt.

===> 04.05.2020-20:00 <===

Da die Dauer des Tiefschlafs sehr ungenau ist (bis zu 10%), ist nun eine Kalibrierung eingefügt. 
Siehe Konstante "SchlafZeitMultiplikator" in "setup_daten.h". Zur Berechnung des Wertes befindet 
sich im Verzeichnis "utility" eine Tabellenkalkulation.

===> 29.04.2020-16:00 <===

ReadMe.pdf erstellt










