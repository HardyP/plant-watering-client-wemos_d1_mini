$fn=120;

Anzahl=2;
ZL1=3;  // Dicke
YL1=80.0; // Laenge
A1DI=6.5; // Durchmesser Innen
A1DO=12.0; // Durchmesser Aussen

module DoIt() 
{
    difference()
    {
        hull()
        {
            cylinder(h=ZL1,d=A1DO,center=false);
            translate([0,30,0])  cylinder(h=ZL1,d=A1DO,center=false);
            translate([0,YL1,0])  cylinder(h=0.5,d=2,center=false);
            
        }
         translate([0,0,0])  cylinder(ZL1*3,d=A1DI,center=true);
    }
}
//DoIt();
for (i=[0:Anzahl-1]) translate ([i*A1DO*3,0,0]) DoIt();