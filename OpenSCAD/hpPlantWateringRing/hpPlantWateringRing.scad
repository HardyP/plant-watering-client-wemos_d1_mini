DoIt=1; // 1=Ring, 2=Support, 3=Klammer, 4=SchlauchSupport

RD=200; // Ring diameter --- Ring-Durchmesser
BD=1.8; // Hole diameter --- Bohr-Durchmesser
RU=3.0; // Ring Bottom --- Ring-Unterseite
RHG=10.0; // Ring height --- Ringhoehe
OW=60.0; // Ring opening angle --- Oeffungswinkel des Rings
BU=false; // Printing holes on the underside --- Bohrungen auf Unterseite drucken
BI=true; // Printing holes on the innerside --- Bohrungen auf Innenseite drucken
BA=4; // Number of holes per leg --- Anzahl Bohrungen je Schenkel
BW=30; // Angular distance of the holes from the water connection --- Winkelabstand der Bohrungen vom Wasseranschluss

ADI=4.8;  // Connection inner diameter --- Anschluss Durchmesser Innen
ADO=7.1;  // Connection outer diameter --- Anschluss Durchmesser Aussen
AL=16.0;  // Connection Length --- Anschluss Laenge

/*
// Aquarium Air Hose --- Aquarium-Luftschschlauch
ADI=3.1;  // Connection inner diameter --- Anschluss Durchmesser Innen
ADO=5.1;  // Connection outer diameter --- Anschluss Durchmesser Aussen
AL=16.0;  // Connection Length --- Anschluss Laenge
*/

SA=2; // Number of support ---  Anzahl der Stuetzen
SD=3;  // support thickness --- Stuetzendicke
SB=11.0; // Support width --- Stuetzenbreite
SL=120.0; // Support length --- Stuetzenlaenge 

BB=10.0; // Bracket width --- Klammerbreite
BL=120.0; // Bracket length --- Klammerlaenge 
BSB=2.5; // Bracket thickness --- Klammerdicke
BIF=ADO+3.0; 

HSD=3;  // Hose support thickness --- Schlauchstuetzendicke
HSB=ADO+6; // Hose support width --- Schlauchstuetzenbreite
HSL=120.0; // Hose support length --- Schlauchstuetzenlaenge 

/* Please do not make any changes from here on */
/* Ab hier bitte keine Veraenderungen vornehmen */
$fn=120;
RDA=2.7; // Rounding Outside --- Rundung Aussen

RDI=RDA/2.2; // Rounding inside --- Rundung Innen
RH=RHG-RDA; // Ring height --- Ringhoehe
WD=(RDA-RDI)/2; // WandDicke

module Support(Anzahl) 
{
    for (i=[0:Anzahl-1]) translate ([i*SB*2,0,0]) 
    {
        difference()
        {
            hull()
            {
                cylinder(h=SD,d=SB,center=false);
                translate([0,SL-50,0])  cylinder(h=SD,d=SB,center=false);
                translate([0,SL,0])  cylinder(h=0.5,d=2,center=false);
            }
            translate([0,2.85,SD/2]) 
                rotate([0,0,180]) 
                    linear_extrude(height=SD*2,center=true,convexity=10,twist=0)
                        Profile(RDA*1.3);
        }
    }
}

module Klammer()
{
    difference()
    {
        union()
        {
            translate([BIF/2+BSB,0,0]) cube([BL-BIF/2-BSB,BIF+BSB*2,BB],center=false);
            translate([BIF/2+BSB,BIF/2+BSB,0]) cylinder(h=BB,d=BIF+BSB*2,center=false);
        }
        union()
        {
            translate([BIF/2+BSB,BSB,0]) cube([BL-BIF/2+BSB-30.0,BIF,BB],center=false);
            translate([BIF/2+BSB,BIF/2+BSB,0]) cylinder(h=BB,d=BIF,center=false);
             translate([BL,BIF/2+BSB,BB/2+1])  rotate([0,20,0]) cube([SL,BIF+BSB*2,BB],center=true);
           hull()
            {
                translate([BL-BIF/2+BSB-30.0,BIF/2+BSB,0]) cylinder(h=BB,d=BIF,center=false);
                translate([BL+BIF+BSB,BIF/2+BSB,0]) cylinder(h=BB,d=BIF+BSB*2-1,center=false);
            }
        }
    }
}

module SchlauchSupport() 
{
   {
        difference()
        {
            hull()
            {
                cylinder(h=HSD,d=HSB,center=false);
                translate([0,HSL-50,0])  cylinder(h=HSD,d=HSB,center=false);
                translate([0,HSL,0])  cylinder(h=0.5,d=2,center=false);
            }
        cylinder(h=HSD,d=ADO+3.0,center=false);
        }
    }
}

module Profile(Rundung)
{
    hull()
    {
        translate([-RU/2,-RH/2,0]) circle(d=Rundung); 
        translate([RU/2,-RH/2,0]) circle(d=Rundung); 
        translate([-RU/2,RH/4,0]) circle(d=Rundung); 
        translate([RU/2,RH/4,0]) circle(d=Rundung); 
        translate([0,RH/2,0]) circle(d=Rundung); 
    }
}

module BohrungUnten()
{
    for(step=[BW:(180-OW/2-BW-5)/(BA-1):180-OW/2]) 
        rotate([0,0,step]) 
            translate([RD/2,0,-RH/2]) 
                color("Red",1.0) 
                    cylinder(h=RH,d=BD,center=true);
    for(step=[360-BW:-(180-OW/2-BW-5)/(BA-1):180+OW/2]) 
        rotate([0,0,step]) 
            translate([RD/2,0,-RH/2]) 
                color("Red",1.0) 
                    cylinder(h=RH,d=BD,center=true);
}

module BohrungInnen()
{
    for(step=[BW:(180-OW/2-BW-5)/(BA-1):180-OW/2]) 
        rotate([0,0,step]) 
            translate([RD/2-RU/2,0,-RH/5]) 
                rotate([0,90,0]) 
                    color("Red",1.0) 
                        cylinder(h=RH/2,d=BD,center=true);
    for(step=[360-BW:-(180-OW/2-BW-5)/(BA-1):180+OW/2]) 
        rotate([0,0,step]) 
            translate([RD/2-RU/2,0,-RH/5]) 
                rotate([0,90,0]) 
                    color("Red",1.0) 
                        cylinder(h=RH/2,d=BD,center=true);
}

module Ring()
{
   difference()
    {
        union()
        {
            rotate_extrude(convexity=10)
                translate([RD/2, 0])  
                {
                    difference()
                    {
                        Profile(RDA);
                        Profile(RDI);
                    }
                }
            translate([RD/2+RH/5,0,-1])  rotate([0,90,0]) cylinder(h=AL,d=ADO,center=false);
            translate([RD/2+RH/5,0,-1]) rotate([0,90,0]) cylinder(h=AL/3,d1=ADO*1.4,d2=ADO,center=false);
        }
        union()
        {
            hull() for(step=[180-OW/2:1:180+OW/2]) rotate([0,0,step]) translate([RD/2-RH/2,0,-RH]) cube([RH,0.1,RH*2],center=false);
            if (BU) BohrungUnten();
            if (BI) BohrungInnen();
            translate([RD/2+RH/5,0,-1])  color("Red",1.0)  rotate([0,90,0]) cylinder(h=AL/3,d1=ADI*1.5,d2=ADI,center=false);
            translate([RD/2+RH/5,0,-1]) color("Red",1.0)   rotate([0,90,0]) cylinder(h=AL*1.2,d=ADI,center=false);
            translate([RD/2+RH/5,0,-RH/2-RDA/2-2])  color("Red",1.0) cylinder(h=2,d=50,center=false);
        }
    }
    rotate([0,0,180-OW/2-0.2]) 
        translate([RD/2,0,0]) 
            rotate([90,0,0]) 
                linear_extrude(height=WD,center=true,convexity=10,twist=0)
                    Profile(RDA);
    rotate([0,0,180+OW/2+0.3]) 
        translate([RD/2,0,0]) 
            rotate([90,0,0]) 
                linear_extrude(height=WD*1.2,center=true,convexity=10,twist=0)
                    Profile(RDA);
}

if (DoIt>=1)
{
    if (DoIt==1) rotate([0,0,45]) Ring();
    if (DoIt==2) Support(SA);
    if (DoIt==3) rotate([0,0,90]) Klammer();
    if (DoIt==4) SchlauchSupport();
}   
    

