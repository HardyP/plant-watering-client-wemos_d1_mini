#define UseSoftAP // Benutzung des Servers als Access Point
const char* addr="192.168.1.50"; // IP des Pflanzen-Servers
unsigned int UdpPort=4097;  // Udp Port des Pflanzen-Servers
const float OffsetVccMess=0.99; // Korrekturwert zur Messung der Akkuspannung
const int hWartenBisWasserMax=24*60*60; // 24 Stunden, maximale Wartezeit bis erneut Wasser gegeben wird
const int hWartenBisWasserSoll=60*60; // Wartezeit bis erneut Wasser gegeben wird bei Start de Clienten
const int ServerErrorExtraSchlaf=1*60; // 1 Minute, Schlafzeit wenn keine Verbindung zum Pflanzen-Server
const int hSchlafZeitMax=61*60; // 60 Minuten, maximale Schlafzeit des Clienten (ESP8266 kann maximal ca. 70Minuten schlafen)
//const int hSchlafZeitSoll=10*60; // 10 Minuten, Schlafzeit beim Start des Clienten
const int hSchlafZeitSoll=3*60; // 10 Minuten, Schlafzeit beim Start des Clienten
const int hPumpZeitMax=60; // 60 Sekunden, maximale Pumpzeit
const int hPumpZeitSoll=5; // 5 Sekunden, Pumpzeit beim Start des Clienten
const float lowVCC=2.9; // Minimale Akkuspannung, darunter Dauerschlaf
//const int SchlafZeitMultiplikator=1000*1e3; // ohne Korrektur = 1e6 
const int SchlafZeitMultiplikator=1055*1e3; // Korrektur fuer Client: 115152227180
//const int SchlafZeitMultiplikator=1065*1e3; // Korrektur fuer Client: 105203220180
//const int SchlafZeitMultiplikator=1057*1e3; // Korrektur fuer Client: 15616612116203
//const int SchlafZeitMultiplikator=1064*1e3; // Korrektur fuer Client: 192180220180
//const int SchlafZeitMultiplikator=1060*1e3; // Korrektur fuer Client: 1071712116203
//const int SchlafZeitMultiplikator=1059*1e3; // Korrektur fuer Client: 323812116203
