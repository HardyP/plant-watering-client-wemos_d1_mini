Breite=8.0;
Laenge=50.0;
Dicke=1.7;
OeseInnen=12.0;
OeseAussen=OeseInnen+Dicke*2;
OeseGeschlossen=false;
AufhaengerInnen=6.0;
AufhaengerAussen=AufhaengerInnen+Dicke*2;
AufhaengerLaenge=AufhaengerAussen*2.5;

$fn=120;

module Ring(Innen,Aussen)
{
    difference()
    {
        cylinder(h=Breite,d=Aussen);
        cylinder(h=Breite,d=Innen);
    }
}

module DoIt();
{
    intersection()
    {
        union()
        {
            cube([OeseAussen/2,OeseAussen/2,Breite]);
            translate([0,-OeseAussen/2,0]) cube([OeseAussen/2,OeseAussen/2,Breite]);
            translate([-OeseAussen/2,-OeseAussen/2,0]) cube([OeseAussen/2,OeseAussen/2,Breite]);
        }
        translate([0,0,0])  Ring(OeseInnen,OeseAussen);
    }
    if (OeseGeschlossen)
    {
        translate([-OeseAussen/2,OeseInnen/2,0])  cube([OeseAussen/2,Dicke,Breite]);
    }
    translate([-OeseAussen/2,0,0]) cube([Dicke,Laenge-OeseAussen/2-AufhaengerAussen/2,Breite]);
    translate([-OeseAussen/2-AufhaengerInnen/2,Laenge-OeseAussen/2-AufhaengerAussen/2,0]) 
    {
        intersection()
        {
            union()
            {
                translate([-AufhaengerAussen/2,0,0]) cube([AufhaengerAussen/2,AufhaengerAussen/2,Breite]);
                translate([0,0,0]) cube([AufhaengerAussen/2,AufhaengerAussen/2,Breite]);
            }
            rotate([0,0,0]) Ring(AufhaengerInnen,AufhaengerAussen);
        }
        translate([-AufhaengerAussen/2,-AufhaengerLaenge,0]) cube([Dicke,AufhaengerLaenge,Breite]);   
    }
}

