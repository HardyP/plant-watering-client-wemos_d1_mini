$fn=120;

Anzahl=2;
ZL1=3;  // Dicke
YL1=80.0; // Laenge
A1DI=3.0; // Anschluss 1 Durchmesser Innen
A1DO=5.0; // Anschluss 1 Durchmesser Aussen
A1L=15.0; // Anschluss 1 Laenge
RDA=1.0;

module DoIt() 
{
     translate([0,0,ZL1])  cylinder(h=A1L,d1=A1DO,d2=A1DI,center=false);
    hull()
    {
        cylinder(h=ZL1,d=A1DO*2,center=false);
        translate([0,30,0])  cylinder(h=ZL1,d=A1DO*2,center=false);
        translate([0,YL1,0])  cylinder(h=0.5,d=A1DI,center=false);
        
    }
}
//DoIt();
for (i=[0:Anzahl-1]) translate ([i*A1DO*5,0,0]) DoIt();