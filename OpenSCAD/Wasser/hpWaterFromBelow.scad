DoIt=2; 

Breite=50;
Laenge=125;
HakenLaenge=20;
HakenDurchmesser=14;
HakenDicke=1.7;
WasserEinlassOben=10;
WasserAuslassUnten=4;
ADI=3.1; // Anschluss 1 Durchmesser Innen
ADO=5.1; // Anschluss 1 Durchmesser Aussen
AL=15.0; // Anschluss 1 Laenge

// Ab hier nicht mehr aendern
HB=Breite; 
HD=HakenDicke;
HDO=HakenDurchmesser;
HLO=HakenLaenge; 
HLU=Laenge-HakenLaenge; 
WEO=WasserEinlassOben;
WAU=WasserAuslassUnten;

$fn=120;

module Ring(Innen,Aussen)
{
    difference()
    {
        cylinder(h=HB,d=Aussen);
        cylinder(h=HB,d=Innen);
    }
}

module Links()
{
    intersection()
    {
        translate([-HDO/2,HLO-HDO/2,0]) Ring(HDO,HDO+HD*2);
        translate([-HDO/2,HLO,HB/2]) cube([HDO+HD*2,HDO,HB],center=true);
    }
    hull()
    {
        translate([HD/2,0,HB/2]) cylinder(h=HB,d=HD,center=true);
        translate([HD/2,HLO-HDO/2,HB/2]) cylinder(h=HB,d=HD,center=true);
     }
    hull()
    {
       translate([-HDO-HD+HD/2,HLO/2,HB/2]) cylinder(h=HB,d=HD,center=true);
       translate([-HDO-HD+HD/2,HLO-HDO/2,HB/2]) cylinder(h=HB,d=HD,center=true);
     }
    difference()
     {
         union()
         {
            translate([-WEO/2+HD/2,-ADO*1.8+HD,HB])  cylinder(h=AL,d=ADO,center=false);
            translate([-WEO/2+HD/2,-ADO*1.8+HD,HB])   cylinder(h=AL/3,d1=ADO*1.8,d2=ADO,center=false);
            hull()
            {
                translate([HD/2,0,HB/2]) cylinder(h=HB,d=HD,center=true);
                translate([HD/2-WEO,-5,HB/2]) cylinder(h=HB,d=HD,center=true);
                translate([HD/2,-HLU,HB/2]) cylinder(h=HB,d=HD,center=true);
                translate([HD/2-WAU,-HLU,HB/2]) cylinder(h=HB,d=HD,center=true);
            }
         }
        union()
        {
            translate([-WEO/2+HD/2,-ADO*1.8+HD,HB-1])  cylinder(h=AL+2,d=ADI,center=false);
            translate([0,0,0]) hull()
            {
                translate([HD/2-HD,-HD-HD/2,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-WEO+HD,-5-HD/2,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-HD,-HLU+HD,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-WAU+HD,-HLU+HD,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
            }
            translate([0,0,0]) hull()
            {
                translate([HD/2-HD,-HLU+20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-WAU+HD,-HLU+20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-HD,-HLU+HD-20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-WAU+HD,-HLU+HD-20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
            }
             translate([0,0,0]) hull()
            {
                translate([HD/2-HD,-HLU+20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-WAU+HD,-HLU+20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-HD,-HLU+HD-20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
                translate([HD/2-WAU+HD,-HLU+HD-20,HB/2]) cylinder(h=HB-HD*2,d=HD,center=true);
            }
               translate([0,0,0]) hull()
            {
                translate([-HD-WEO,-HLU+30,HB/2]) rotate([0,90,0]) cylinder(h=WEO,d=HD,center=false);
                translate([-HD-WEO,-HLU-5,HB/2-15]) rotate([0,90,0]) cylinder(h=WEO,d=HD,center=false);
                translate([-HD-WEO,-HLU-5,HB/2+15]) rotate([0,90,0]) cylinder(h=WEO,d=HD,center=false);
            }
       }
    }
}

module Rechts()
{
      mirror([1,0,0]) Links();
}

module Clip()
{
    intersection()
    {
        translate([-HDO/2,HLO-HDO/2-8,0]) Ring(HDO,HDO+HD*2);
        translate([-HDO/2,HLO-8,HB/2]) cube([HDO+HD*2,HDO,HB],center=true);
    }
    hull()
    {
        translate([HD/2,0,HB/2]) cylinder(h=HB,d=HD,center=true);
        translate([HD/2,HLO-HDO/2-8,HB/2]) cylinder(h=HB,d=HD,center=true);
     }
    hull()
    {
        translate([HD/2,0,HB/2]) cylinder(h=HB,d=HD,center=true);
        translate([HD/2-10,-2,HB/2]) cylinder(h=HB,d=HD,center=true);
    }
     hull()
    {
        translate([HD/2-12,0,HB/2]) cylinder(h=HB,d=HD,center=true);
        translate([HD/2-10,-2,HB/2]) cylinder(h=HB,d=HD,center=true);
    }
 
}

module Set()
{
         translate([-XA/2,0,]) Aufhaenger();
         translate([50,0,]) Haken();
         translate([-50,0,]) Haken();
}

if (DoIt==1) {Links();} 
else if (DoIt==2) {Rechts();} 
else {Clip();}







   
