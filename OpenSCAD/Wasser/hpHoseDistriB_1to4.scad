$fn=120;

XL=30; // Laenge
YL1=16;  // Breite
YL2=16;  // Breite oben
ZL=16; // Hoehe
WD=2.49; // WandDicke
RDA=2.49;
RDI=0.1;

A1DI=3.1; // Anschluss 1 Durchmesser Innen
A1DO=5.1; // Anschluss 1 Durchmesser Aussen
A1L=15.0; // Anschluss 1 Laenge
A2DI=4.6; // Anschluss 2 Durchmesser Innen
A2DO=7.1; // Anschluss 2 Durchmesser Aussen
A2L=15.0; // Anschluss 2 Laenge



 

module DoIt() 
{
    difference() 
    {
        union()
        {
            color("LightGreen",0.5) hull()
            {
                translate([-XL/2+RDA,-YL1/2+RDA,-ZL/2+RDA]) sphere(RDA);
                translate([XL/2-RDA,-YL1/2+RDA,-ZL/2+RDA]) sphere(RDA);
                translate([-XL/2+RDA,+YL1/2-RDA,-ZL/2+RDA]) sphere(RDA);
                translate([XL/2-RDA,+YL1/2-RDA,-ZL/2+RDA]) sphere(RDA);
                
                translate([-XL/2+RDA,-YL2/2+RDA,ZL/2-RDA]) sphere(RDA);
                translate([XL/2-RDA,-YL2/2+RDA,ZL/2-RDA]) sphere(RDA);
                translate([-XL/2+RDA,+YL2/2-RDA,ZL/2-RDA]) sphere(RDA);
                translate([XL/2-RDA,+YL2/2-RDA,ZL/2-RDA]) sphere(RDA);
            }
           translate([XL/4,-YL1/2+RDA,0])   rotate([90,0,0]) cylinder(h=A1L,d=A1DO,center=false);
           translate([-XL/4,-YL1/2+RDA,0])   rotate([90,0,0]) cylinder(h=A1L,d=A1DO,center=false);
           translate([XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2,d2=A1DO,center=false);
           translate([-XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2,d2=A1DO,center=false);

          rotate([0,0,180]) 
            {
               translate([XL/4,-YL1/2+RDA,0])   rotate([90,0,0]) cylinder(h=A1L,d=A1DO,center=false);
                translate([-XL/4,-YL1/2+RDA,0])   rotate([90,0,0]) cylinder(h=A1L,d=A1DO,center=false);
                translate([XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2,d2=A1DO,center=false);
                translate([-XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2,d2=A1DO,center=false);
           }
         
           translate([YL1/2+RDA,0,0])   rotate([90,0,90]) cylinder(h=A2L,d=A2DO,center=false);
          translate([YL1/2+RDA+3,0,0])   rotate([90,0,90]) cylinder(h=A2L/3,d1=A2DO*2,d2=A2DO,center=false);
        }
        union()
        {
             translate([0,0,0]) color("Red",1.0) hull()
            {
                translate([-XL/2+RDI+WD,-YL1/2+RDI+WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([XL/2-RDI-WD,-YL1/2+RDI+WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([-XL/2+RDI+WD,+YL1/2-RDI-WD,-ZL/2+RDI+WD]) sphere(RDI);
                translate([XL/2-RDI-WD,+YL1/2-RDI-WD,-ZL/2+RDI+WD]) sphere(RDI);
                
                translate([-XL/2+RDI+WD,-YL2/2+RDI+WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,-YL2/2+RDI+WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([-XL/2+RDI+WD,+YL2/2-RDI-WD,ZL/2-RDI-WD]) sphere(RDI);
                translate([XL/2-RDI-WD,+YL2/2-RDI-WD,ZL/2-RDI-WD]) sphere(RDI);
            }
           translate([XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) color("Red",1.0) cylinder(h=A1L*2,d=A1DI,center=true);
           translate([-XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) color("Red",1.0) cylinder(h=A1L*2,d=A1DI,center=true);
          rotate([0,0,180]) 
            {
            translate([XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) color("Red",1.0) cylinder(h=A1L*2,d=A1DI,center=true);
           translate([-XL/4,-YL1/2+RDA-1,0])   rotate([90,0,0]) color("Red",1.0) cylinder(h=A1L*2,d=A1DI,center=true);
            }
           translate([YL1/2+RDA+1,0,0])   rotate([90,0,90]) color("Red",1.0) cylinder(h=A2L*2,d=A2DI,center=true);
            
           translate([XL/4,-YL1/2+RDA+WD,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2-2.0,d2=A1DO-2.0,center=false);
           translate([-XL/4,-YL1/2+RDA+WD,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2-2.0,d2=A1DO-2.0,center=false);
           rotate([0,0,180]) 
            {
           translate([XL/4,-YL1/2+RDA+WD,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2-2.0,d2=A1DO-2.0,center=false);
           translate([-XL/4,-YL1/2+RDA+WD,0])   rotate([90,0,0]) cylinder(h=A1L/3,d1=A1DO*2-2.0,d2=A1DO-2.0,center=false);
                
            }
           translate([YL1/2+RDA-WD+4.3,0,0])   rotate([90,0,90]) cylinder(h=4.0,d1=A2DO+2.4,d2=A2DO-3,center=false);
      }
    }
}

DoIt();